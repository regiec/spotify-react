import React, { ChangeEvent, Component, PureComponent } from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
  onCancel(): void
  onSave(draft:Playlist): void
  playlist: Playlist,
}

interface State {
  playlist: Playlist,
}

class PlaylistEditForm extends PureComponent<Props, State> {

  state: State = {
    playlist: {
      id: '123',
      name: 'My fake playlist',
      public: true,
      description: 'My favorite one'
    },
  }

  constructor(props: Props) {
    super(props)
    this.state.playlist = this.props.playlist
  }


  static getDerivedStateFromProps(nextProps: Props, nextState: State) {
    return {
      playlist: nextProps.playlist.id === nextState.playlist.id ?
        nextState.playlist : nextProps.playlist
    }
  }

  // shouldComponentUpdate(nextProps: Props, nextState: State){
  //   // {} == {}
  //   return this.state.playlist !== nextState.playlist ||
  //   this.props.playlist !== nextProps.playlist
  // }

  handleChange = (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    // this.state.playlist.name = event.target.value // Dont modify reference
    const target = event.target
    const fieldName = target.name

    const value = target.type === 'checkbox' ? (target as HTMLInputElement).checked : target.value
    // Copy on change
    const playlist = {
      ...this.state.playlist,
      [fieldName]: value
    }

    this.setState({
      // playlist: playlist
      playlist
    })
  }

  getSnapshotBeforeUpdate() {
    return { pozycjaKursora: 124 }
  }

  componentDidUpdate(prevProps: Readonly<Props>, prevState: Readonly<State>, snapshot?: any) {
    // console.log(snapshot)
  }

  componentDidMount() {
    // this.setState({
    //     playlist: this.props.playlist
    // })
    this.inputRef.current?.focus()
  }

  inputRef = React.createRef<HTMLInputElement>()

  save = () => {
    this.props.onSave(this.state.playlist)
  }

  render() {
    const playlist = this.state.playlist

    return (
      <div>
        <pre>{JSON.stringify(this.state.playlist, null, 2)}</pre>
        {/* .form-group>label{Name:}+input.form-control */}

        <div className="form-group">
          <label>Name:</label>
          {/* // TODO: FOCUS */}
          {/* ref={(e) => console.log('hello',e)} */}
          <input type="text" className="form-control" value={playlist.name} name="name"
           onChange={this.handleChange} ref={this.inputRef} />

          <span>{this.state.playlist.name.length} / 170</span>
        </div>
        {/* .form-group>label>input[type=checkbox]+{ Public} */}
        <div className="form-group">
          <label><input type="checkbox" checked={playlist.public} name="public" onChange={this.handleChange} /> Public</label>
        </div>
        {/* .form-group>label{Description:}+textarea.form-control */}
        <div className="form-group">
          <label>Description:</label>
          <textarea className="form-control" value={playlist.description} name="description" onChange={this.handleChange} />
        </div>

        <button className="btn btn-danger" onClick={this.props.onCancel}>Cancel</button>
        {/* TODO;SAVE */}
        <button className="btn btn-success" onClick={this.save} >Save</button>
      </div>
    )
  }
}

export default PlaylistEditForm
