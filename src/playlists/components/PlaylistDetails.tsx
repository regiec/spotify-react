
// tsrafce 
import React from 'react'
import { Playlist } from '../../core/model/Playlist';

interface Props {
    playlist?: Playlist
    onEdit?(/* event: React.MouseEvent<HTMLButtonElement> */): void
}

const PlaylistDetails = React.memo(({ playlist, onEdit }: Props) => {

    if (!playlist) {
        return <p>Please select playlist</p>
    }

    return (
        <div>
            {/* dl>(dt{Name:}+dd{info})*3 */}
            <dl>
                <dt>Name:</dt>
                <dd>{playlist.name}</dd>

                <dt>Public:</dt>
                <dd style={{
                    color: playlist.public ? 'green' : 'red'
                }}>{playlist.public ? 'Yes' : 'No'}</dd>

                <dt>Description:</dt>
                <dd>{playlist.description}</dd>
            </dl>

            {onEdit && <button className="btn btn-info" onClick={onEdit}>Edit</button>}
        </div>
    )
}, (prevProps: Readonly<Props>, nextProps: Readonly<Props>) => {
    return prevProps.playlist === nextProps.playlist
})

export default PlaylistDetails
