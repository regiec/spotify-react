import React, { Component, PureComponent } from 'react'
import { Playlist } from '../../core/model/Playlist'

interface Props {
  playlists: Playlist[]
  activeId?: Playlist['id']
  onSelect(playlist_id: Playlist['id']): void
  onRemove(playlist_id: Playlist['id']): void
}

const PlaylistList = React.memo(({
  onSelect, playlists, activeId, onRemove
}: Props) => {

  const remove = (event: React.MouseEvent, playlist_id: Playlist['id']) => {
    event.stopPropagation()
    onRemove(playlist_id)
  } 

  return <div>

    <div className="list-group">
      {playlists.map((playlist, index) => <div key={playlist.id}

        onClick={() => onSelect(playlist.id)}

        className={`list-group-item ${activeId === playlist.id ? 'active' : ''}`}>

        {index + 1}. {playlist.name}

        <span className="close" onClick={(e) => remove(e, playlist.id)}>&times;</span>
      </div>)}
    </div>
  </div >
})

export default PlaylistList

// interface State {
// }

// class PlaylistList extends PureComponent<Props, State> {

//   selectPlaylist = (playlist_id: Playlist['id']) => {
//     this.props.onSelect(playlist_id)
//   }

//   render() {
//     return (
//       <div>
//         {/* .list-group>.list-group-item*3{Text} */}
//         <div className="list-group">
//           {this.props.playlists.map((playlist, index) => <div key={playlist.id}

//             onClick={() => this.selectPlaylist(playlist.id)}

//             className={`list-group-item ${this.props.activeId === playlist.id ? 'active' : ''}`}>

//             {index + 1}. {playlist.name}
//           </div>)}
//         </div>
//       </div>
//     )
//   }
// }

// export default PlaylistList