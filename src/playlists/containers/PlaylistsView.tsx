/* tsrce */
import React, { Component, Fragment } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { Playlist } from '../../core/model/Playlist'
import PlaylistDetails from '../components/PlaylistDetails'
import PlaylistEditForm from '../components/PlaylistEditForm'
import PlaylistList from '../components/PlaylistList'

interface Props extends RouteComponentProps<{
  playlist_id: string
}> {

}

interface State {
  playlists: Playlist[]
  selected?: Playlist,
  mode: 'edit' | 'details'
}

class PlaylistsView extends Component<Props, State> {
  state: State = {
    playlists: [
      {
        id: '123',
        name: 'My playlist 123',
        public: false,
        description: 'My favorite one'
      },
      {
        id: '234',
        name: 'My playlist 243',
        public: true,
        description: 'My favorite one'
      },
      {
        id: '345',
        name: 'My playlist 354',
        public: false,
        description: 'My favorite one'
      }
    ],
    mode: 'details'
  }

  edit = () => {
    this.setState({ mode: 'edit' })
  }

  cancel = () => {
    this.setState({ mode: 'details' })
  }

  removePlaylist = (playlist_id: Playlist['id']) => {
    this.setState(prevState => ({
      mode: 'details',
      selected: prevState.selected?.id === playlist_id ? undefined : prevState.selected,
      playlists: prevState.playlists.filter(p => p.id !== playlist_id)
    }))
  }

  // static getDerivedStateFromProps() { return {} }

  componentDidMount() {
    this.updatePlaylistFromUrl()
  }

  componentDidUpdate() {
    this.updatePlaylistFromUrl()
  }

  private updatePlaylistFromUrl() {
    const playlist_id = this.props.match.params.playlist_id

    if (this.state.selected?.id !== playlist_id) {
      this.setState({
        selected: this.state.playlists.find(p => p.id === playlist_id)
      })
    }
  }

  save = (draft: Playlist) => {
    this.setState(prevState => ({
      mode: 'details',
      selected: draft,
      playlists: prevState.playlists.map(p => p.id === draft.id ? draft : p)
    }))
  }

  changePlaylist = (playlist_id: Playlist['id']) => {
    // this.props.history.push('/playlists/' + playlist_id)
    this.props.history.replace('/playlists/' + playlist_id)
  }

  render() {
    return (
      <div>

        <div className="row">
          <div className="col">
            <PlaylistList
              playlists={this.state.playlists}
              activeId={this.state.selected?.id}
              onRemove={this.removePlaylist}
              onSelect={this.changePlaylist} />

          </div>
          <div className="col">

            {this.state.mode === 'details' && <>
              <PlaylistDetails playlist={this.state.selected} onEdit={this.edit} />

            </>}

            {this.state.mode === 'edit' && this.state.selected && <>
              <PlaylistEditForm playlist={this.state.selected} onCancel={this.cancel} onSave={this.save} />
            </>}

          </div>
        </div>

      </div>
    )
  }
}

export default PlaylistsView
