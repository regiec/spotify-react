import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { initAuth } from './core/services/Auth';

import {
  BrowserRouter as Router
} from "react-router-dom";
import UserContextProvider from './core/contexts/UserContext';
import AlbumDetails from './search/containers/AlbumDetails';
import PlaylistsView from './playlists/containers/PlaylistsView';

initAuth()


ReactDOM.render(
  <React.StrictMode>

    <UserContextProvider>
      <Router>
        <App />
      </Router>
    </UserContextProvider>

  </React.StrictMode>,
  document.getElementById('root')
);


(window as any).createAlbumWidget = (CSSselector = 'root') => {
  ReactDOM.render(<Router><AlbumDetails /></Router>, document.querySelector(CSSselector));
}

// (window as any).createPlaylistWidget = (CSSselector = 'root') => {
//   ReactDOM.render(<PlaylistsView />, document.querySelector(CSSselector));
// }



// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();



window.React = React
window.ReactDOM = ReactDOM


// interface User {
//   name: string;
//   color: string;
// }

// // const color = 'blue';
// // const user = { name: 'Alicja' }
// const users: User[] = [
//   { name: 'Alice', color: 'blue' },
//   { name: 'Bob', color: 'red' },
//   { name: 'Cat', color: 'teal' },
// ]
// // const user = users[0]

// debugger

// const Person = (props: { user: User }) => <p  id={props.user.name} style={{ color: 'white', backgroundColor: props.user.color }}>
//   {props.user.name} ma kota, <input /> i rybki
// </p>

// const div = <div>
//   {
//     users.map(user => <div key={user.name}>
//       {/* {Person({ user: user })} */}
//       <Person user={user}/>
//     </div>)
//   }
// </div>

// ReactDOM.render(div, document.getElementById('root'))

// function makePerson(props: { user: User, children?: React.ReactNode[] }) {
//   return React.createElement('p', {
//     key: props.user.name,
//     style: {
//       color: 'white',
//       backgroundColor: props.user.color
//     },
//     className: 'placki'
//   },
//     props.user.name + ' ma kota',
//     React.createElement('input'),
//     'i rybki'
//   );
// }

// const elementy = users.map(user => {
//   return makePerson({ user: user})
// })

// const div = React.createElement('div', { id: 123 },
//   elementy
// )
