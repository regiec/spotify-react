import axios from "axios";


/* Axios global interceptors */
axios.interceptors.request.use(config => {
    config.headers['Authorization'] = 'Bearer ' + getToken()
    return config
}, err => err)

axios.interceptors.response.use(res => res, err => {
    if (axios.isCancel(err)) {
        return Promise.reject(new Error('Request Cancelled'))
    }
    if (err.response.status === 401) {
        login()
    }
    // if (err.response?){
    //     return err.reqeust
    // }
    return Promise.reject(new Error(err.response?.data?.error.message))
})

/* Auth service */

let token: string | null = null;

export const getToken = () => {
    return token
}

export const initAuth = () => {

    const access_token = new URLSearchParams(window.location.hash.substr(1)).get('access_token')
    if (access_token) {
        token = access_token
        window.sessionStorage.setItem('token', JSON.stringify(token))
        window.location.hash = ''
        return
    }
    try {
        token = JSON.parse(window.sessionStorage.getItem('token') || '')
    } catch (ee) { }

    if (!token) {
        // login()
    }
}


export const logout = () => {
    token = null
    window.sessionStorage.removeItem('token')
}

export const login = () => {


    const params = new URLSearchParams({
        client_id: '58e631afac35452fb3fde1f9500e3f40',
        response_type: 'token',
        redirect_uri: 'http://localhost:3000/'
    })

    const url = 'https://accounts.spotify.com/authorize?' + params

    window.location.replace(url)
}

