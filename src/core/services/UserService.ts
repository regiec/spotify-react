import axios from "axios"
import { UserProfile } from "../model/UserProfile"


export const getUser = async () => {
    const resp = await axios.get<UserProfile>(`https://api.spotify.com/v1/me`)

    return resp.data
}