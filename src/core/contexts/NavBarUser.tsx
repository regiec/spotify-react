import React, { useContext } from 'react';
import { UserContext } from './UserContext';


export const NavBarUser = () => {
  const { user, login, logout } = useContext(UserContext)
  
  return (
    <nav className="navbar  ml-auto">

      {!user && <span className="navbar-text">
        Welcome, Guest | <a onClick={login}>Login</a>
      </span>}

      {user && <span className="navbar-text">
        Welcome, {user.display_name} | <a onClick={logout}>Logout</a>
      </span>}

    </nav>
  );
};

// export const NavBarUser = () => {

//   return (
//     <UserContext.Consumer>{({ user, login, logout }) => {

//       return <nav className="navbar  ml-auto">

//         {!user && <span className="navbar-text">
//           Welcome, Guest | <a onClick={login}>Login</a>
//         </span>}

//         {user && <span className="navbar-text">
//           Welcome, {user.display_name} | <a onClick={logout}>Logout</a>
//         </span>}

//       </nav>;

//     }}</UserContext.Consumer>
//   );
// };
