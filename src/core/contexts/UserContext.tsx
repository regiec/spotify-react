

// UserProfile

import React, { useContext, useEffect, useState } from "react";
import { UserProfile } from "../model/UserProfile";
import { getToken, logout } from "../services/Auth";
import { getUser } from "../services/UserService";

interface UserCtx {
    user: UserProfile | null,
    login(): void,
    logout(): void,
}
const NoProviderErrFn = () => void {
    return() { throw 'No provider for UserContext'; }
}

export const UserContext = React.createContext<UserCtx>({
    user: null,
    login: NoProviderErrFn,
    logout: NoProviderErrFn,
})

const checkLogin = async () => {
    try {
        if (!getToken()) { return null }
        const user = getUser()
        return user
    } catch (err) {
        return null
    }
}

const UserContextProvider: React.FC = (props) => {

    const [user, setUser] = useState<UserProfile | null>(null)

    useEffect(() => {
        checkLogin().then(setUser)
    }, [])

    const ctx: UserCtx = {
        user,
        login: async () => {
            // setUser({ display_name: 'Placki' } as UserProfile)
            const user = await getUser()
            setUser(user)
        },
        logout: () => {
            logout()
            setUser(null)
        }
    }

    return (
        <UserContext.Provider value={ctx}>
            {props.children}
        </UserContext.Provider>
    )
}

export default UserContextProvider


export const useUser = () => {
    const { user } = useContext(UserContext)
    return user
}