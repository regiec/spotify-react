
export interface Track {
    id: string // | number;
    name: string;
}

export interface Playlist {
    id: string // | number;
    name: string;
    public: boolean;
    description: string;
    tracks?: Track[]
}

// const p: Playlist = {
//     id: '123',
//     name: '123',
//     description: '123',
//     public: true,
// }

// // if (p.tracks) { p.tracks.length }
// const l1 = p.tracks ? p.tracks.length : 0
// const l2 = p.tracks && p.tracks.length
// const l3 = p.tracks?.length
// const l4 = p.tracks?.length ?? 0

// if (dziadek) {
//     if (dziadek.ojciec) {
//         if (dziadek.ojciec.syn) {

// dziadek?.ojciec?.syn

// p.id = '123'
// p.id = 123

// if('string' === typeof p.id){
//     p.id.substring
// }else{
//     p.id.toExponential
// }
