import React from 'react'
import { AlbumView } from '../../core/model/Search'
import AlbumCard from './AlbumCard'

interface Props {
  albums: AlbumView[]
}

const AlbumGrid = ({albums}: Props) => {
  return (
    <div>
      <div className="row row-cols-1 row-cols-sm-4 no-gutters">
        {albums.map((album, i) => <div className="col mb-2" key={album.id}>
          <AlbumCard album={album} />
        </div>)}
      </div>
    </div>
  )
}

export default AlbumGrid
