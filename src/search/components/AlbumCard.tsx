import React from 'react'
import { Link } from 'react-router-dom'
import { Album, AlbumView } from '../../core/model/Search'

interface Props {
  album: AlbumView
}

const AlbumCard = ({ album }: Props) => {
  return (
    <div>
      <div className="card">
        <img src={album.images[0].url} className="card-img-top" alt="..." />

        <div className="card-body">
          <h5 className="card-title">
            <Link to={'/albums/'+album.id}>{album.name}</Link>
          </h5>
        </div>
      </div>
    </div>
  )
}

export default AlbumCard
