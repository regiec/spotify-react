import axios from 'axios';
import { useEffect, useState } from 'react';
import { AlbumView } from '../../core/model/Search';
import { fetchSearchResults } from '../../core/services/MusicSearch';



export function useSearchAlbums(initialQuery: string = 'batman') {

    const [query, setQuery] = useState(initialQuery)
    const [type, setType] = useState('album')

    // Change timestamp to force fetch data from server:
    const [refresh, setRefresh] = useState(Date.now());

    // Results from server:
    const [albums, setAlbums] = useState<AlbumView[]>([]);
    const [message, setMessage] = useState('');
    const [loading, setLoading] = useState(false);


    useEffect(() => {
        const cancelToken = axios.CancelToken.source()
        setLoading(true);
        setAlbums([]);
        setMessage('');
        fetchSearchResults(query, { cancelToken: cancelToken.token })
            .then(albums => setAlbums(albums))
            .catch(err => setMessage(err.message))
            .finally(() => setLoading(false));

        return () => cancelToken.cancel()
    }, [query, refresh]);

    return { albums, message, loading, query, setQuery };
}
