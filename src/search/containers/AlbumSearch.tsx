import axios from 'axios'
import { useEffect, useReducer } from 'react'
import { RouteComponentProps, useHistory, useLocation } from 'react-router-dom'
import { fetchSearchResults } from '../../core/services/MusicSearch'
import AlbumGrid from '../components/AlbumGrid'
import SearchForm from '../components/SearchForm'
import { reducer, searchSuccessAction, searchFailedAction, searchStartAction, initialState } from '../reducers/searchReducer'

interface Props { }


const Loading = () => <div className="d-flex justify-content-center text-primary">
    <div className="spinner-border" role="status">
        <span className="sr-only">Loading...</span>
    </div>
</div>

const AlbumSearch = ({ /* location, history  */ }: Props & RouteComponentProps) => {
    const [{ albums, loading, message, query }, dispatch] = useReducer(reducer, initialState)

    const location = useLocation()
    const history = useHistory()

    const queryParam = new URLSearchParams(location.search.substr(1)).get('q')

    useEffect(() => {
        const cancelToken = axios.CancelToken.source()

        if (!queryParam) { return }
        dispatch(searchStartAction(queryParam))
        fetchSearchResults(queryParam, { cancelToken: cancelToken.token })
            .then(albums => dispatch(searchSuccessAction(albums)))
            .catch(err => dispatch(searchFailedAction(err)))

        return () => cancelToken.cancel()
    }, [queryParam])


    return (
        <div>
            <div className="row">
                <div className="col">
                    <SearchForm onSearch={(query) => history.push('/search?q=' + query)} query={query} />
                </div>
            </div>
            <div className="row">
                <div className="col">
                    {!message && !!albums.length && `Results for "${query}"`}
                    {message && <p className="alert alert-danger">{message}</p>}

                    {loading && <Loading/>}

                    {/* <button onClick={()=>setRefresh(Date.now())}>Refres</button> */}
                    <AlbumGrid albums={albums} />
                </div>
            </div>
        </div>
    )
}

export default AlbumSearch
