import { Reducer } from "react"
import { AlbumView } from "../../core/model/Search"

/* Types  */
interface State {
    query: string
    message: string
    albums: AlbumView[]
    loading: boolean
}
interface SEARCH_START { type: 'SEARCH_START', payload: { query: string } }
interface SEARCH_SUCCESS { type: 'SEARCH_SUCCESS', payload: { albums: AlbumView[] } }
interface SEARCH_FAILED { type: 'SEARCH_FAILED', payload: { message: string } }
interface SEARCH_CLEAR { type: 'SEARCH_CLEAR', payload: {} }

type Actions =
    | SEARCH_START
    | SEARCH_CLEAR
    | SEARCH_SUCCESS
    | SEARCH_FAILED

/* Reducer */
export const initialState: State = {
    query: '',
    albums: [],
    loading: false,
    message: ''
}

export const reducer: Reducer<State, Actions> = (state, action): State => {
    switch (action.type) {
        case 'SEARCH_CLEAR':
            return { ...state, query: '', message: '', albums: [] }
        case 'SEARCH_START':
            return { ...state, query: action.payload.query, loading: true, message: '', albums: [] }
        case 'SEARCH_SUCCESS':
            return { ...state, albums: action.payload.albums, message: '', loading: false }
        case 'SEARCH_FAILED':
            return { ...state, message: action.payload.message, loading: false }

        default:
            return state
    }
}

/* Action Creators */
export const searchSuccessAction = (albums: AlbumView[]): SEARCH_SUCCESS => ({ type: 'SEARCH_SUCCESS', payload: { albums } });
export const searchFailedAction = (error: Error): SEARCH_FAILED => ({ type: 'SEARCH_FAILED', payload: { message: error.message } });
export const searchStartAction = (query: string): SEARCH_START => ({ type: 'SEARCH_START', payload: { query } });
