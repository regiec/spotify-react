## GIT 
cd ..
git clone https://bitbucket.org/ev45ive/altkom-react-marzec.git projekt-react 
cd projekt-react
npm i 
npm start



## Instalacje 
https://marketplace.visualstudio.com/items?itemName=dsznajder.es7-react-js-snippets

https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi

https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype

node -v
v14.15.4

$ npm -v
6.14.6

$ git --version
git version 2.28.0.windows.1

$ code -v
1.54.3

## Create React APP

npm i -g create-react-app
C:\Users\PC\AppData\Roaming\npm\create-react-app -> C:\Users\PC\AppData\Roaming\npm\node_modules\create-react-app\index.js

create-react-app --help 
create-react-app . --template typescript

## Bootstrap CSS
npm i bootstrap
yarn add bootstrap

mkdir -p src/playlists/containers
mkdir -p src/playlists/components

touch src/playlists/containers/PlaylistsView.tsx

touch src/playlists/components/PlaylistList.tsx
touch src/playlists/components/PlaylistDetails.tsx
touch src/playlists/components/PlaylistEditForm.tsx


mkdir -p src/core/model
touch src/core/model/Playlist.ts



mkdir -p src/search/containers
mkdir -p src/search/components
touch src/search/containers/AlbumSearch.tsx
touch src/search/components/SearchForm.tsx
touch src/search/components/AlbumGrid.tsx
touch src/search/components/AlbumCard.tsx


## Axios
npm i axios


## Router
npm install react-router-dom
yarn add react-router-dom

Could not find a declaration file for module 'react-router-dom'. 'C:/Projects/szkolenia/altkom/altkom-react-marzec/node_modules/react-router-dom/index.js' implicitly has an 'any' type.
  Try `npm i --save-dev @types/react-router-dom` if it exists or add a new declaration (.d.ts) file containing `declare module 'react-router-dom';`
npm i --save-dev @types/react-router-dom
yarn add @types/react-router-dom





## Books API 
```ts
await (await fetch('https://www.googleapis.com/books/v1/volumes?q=flowersinauthor:keyes&key= KLUZC ')).json()
```

## Movie API 
```ts
await (await fetch('https://api.themoviedb.org/3/search/movie?api_key= KLUC &query=alice')).json()
```

## Spotify simple AUTH API 
```ts

var client_id = 'adasdasd';
var client_secret = 'asdasdasd';
await (await fetch('https://accounts.spotify.com/api/token',{
    method:'POST',
    headers:{
        'Content-Type':'application/x-www-form-urlencoded',
        'Authorization': `Basic ${btoa(`${client_id}:${client_secret}`)}`
    },
    body: new URLSearchParams({ grant_type: 'client_credentials' }).toString()

})).json()
```

## Wordpress API
https://developer.wordpress.org/rest-api/extending-the-rest-api/