```js
console.log(1)

setTimeout(()=> console.log(2), 0)

Promise.resolve(3).then(console.log)

console.log(4)

now = Date.now()
while( Date.now() < now + 4000) { } 

console.log(5)

```

```js
function echo(msg, cb){

    setTimeout(()=>{
        // response
        cb('Response '+msg)
        
    },2000)

}

echo('test', res => console.log(res));

console.log('dalej...')

VM42427:13 dalej...
undefined
VM42427:11 Response test
```


## Callback pyramid of doom / Callback hell

```js
echo('Alice ', res => {
    echo( res + 'ma kota', res => {
        echo (res + ' a kot ma',res =>  console.log(res) )
    })
});
```

## Promises
```js
function echo(msg){
    return new Promise((resolve)=>{
        
        setTimeout(()=>{
            // response
            resolve('Response '+msg)

        },2000)
    })
}

p = echo('Alice ')
p2 = p.then(a => echo( a + 'ma kota'))
p3 = p2.then(b => echo (b + ' a kot ma'))
p3.then(c => console.log(c))

console.log('dalej...')

VM42925:17 dalej...
undefined
VM42925:15 Response Response Response Alice ma kota a kot ma
```

## Promise error handling

```js
function echo(msg, error){
    return new Promise((resolve, reject)=>{
        
        setTimeout(()=>{
            // response
           error? reject(error) : resolve('Response '+msg)

        },2000)
    })
}

p = echo('Alice ','Ups..')
p2 = p.then(a => echo( a + 'ma kota'), () => 'Nie ma alicji ' )
p3 = p2.then(b => echo (b + ' a kot ma promise', 'errory'))
p4 = p3.catch((e) => 'wystapil blad ' + e)
p4.then(c => console.log(c))

console.log('dalej...')
```

## Promise chain
```js

echo('Alice ','Ups..')
.then(a => echo( a + 'ma kota'), () => 'Nie ma alicji ' )
.then(b => echo (b + ' a kot ma promise', 'errory'))
.catch((e) => 'wystapil blad ' + e)
.then(c => console.log(c))

console.log('dalej...')

```

## Fetch api
```js
fetch('http://localhost:3000/manifest2.json')
.then(res => res.json())
.then(res => console.log(res))

```