
```js
[1,2,3,4,5].reduce((state,x)=>{

    return { 
        ...state,
        counter: state.counter + x 
    }
},{
    counter:0,
    todos:[{name:'redux!'}]
})
```


```js
inc = (payload=1) =>  s => ({...s, counter: s.counter + payload});
dec = (payload=1) =>  s => ({...s, counter: s.counter - payload});
addTodo = (payload) =>  s => ({...s, todos: [...s.todos, {name: payload} ] });


[inc(),inc(2),addTodo('profit!'),dec(1),inc(3)].reduce((state,fn)=>{

    return fn(state)
},{
    counter:0,
    todos:[{name:'redux!'}]
})
```

```js
inc = (payload=1) => ({ type:'INC', payload })
dec = (payload=1) =>  ({ type:'DEC', payload })
addTodo = (payload) => ({ type:'TODO/ADD', payload })


;[inc(),inc(2),addTodo('profit!'),dec(1),inc(3)].reduce((state,action)=>{

    switch(action.type){
        case 'INC': return ({...state, counter: state.counter + action.payload});
        case 'DEC': return ({...state, counter: state.counter - action.payload});
        case 'TODO/ADD': return ({
            ...state, 
            todos:[...state.todos, { name: action.payload} ]
        });
        default: return state;
    }
},{
    counter:0,
    todos:[{name:'redux!'}]
})
{counter: 5, todos: Array(2)}
```

```js
inc = (payload=1) => ({ type:'INC', payload })
dec = (payload=1) =>  ({ type:'DEC', payload })
addTodo = (payload) => ({ type:'TODO/ADD', payload })

reducer = (state,action)=>{

    switch(action.type){
        case 'INC': return ({...state, counter: state.counter + action.payload});
        case 'DEC': return ({...state, counter: state.counter - action.payload});
        case 'TODO/ADD': return ({
            ...state, 
            todos:[...state.todos, { name: action.payload} ]
        });
        default: return state;
    }
}


state = {
    counter:0,
    todos:[{name:'redux!'}]
}
state = reducer(state, inc());
state = reducer(state, inc(2));
state = reducer(state, addTodo('profit!'));
state = reducer(state, dec(1));
state = reducer(state, inc(3));
state;
{counter: 5, todos: Array(2)}
counter: 5
todos: Array(2)
0: {name: "redux!"}
1: {name: "profit!"}

```

## SLices

```js
inc = (payload=1) => ({ type:'INC', payload })
dec = (payload=1) =>  ({ type:'DEC', payload })
addTodo = (payload) => ({ type:'TODO/ADD', payload })


counterReducer = (state = 0,action) => {

    switch(action.type){
        case 'INC': return  state  + action.payload;
        case 'DEC': return  state - action.payload;
        default: return state;
    }
}


reducer = (state,action)=>{


    switch(action.type){
        case 'TODO/ADD': return ({
            ...state, 
            todos:[...state.todos, { name: action.payload} ]
        });
        default: return {
            ...state,
            counter: counterReducer(state.counter, action),
            counter2: counterReducer(state.counter2, action),
        };
    }
}


state = {
    counter:0,
    todos:[{name:'redux!'}]
}
state = reducer(state, inc());
state = reducer(state, inc(2));
state = reducer(state, addTodo('profit!'));
state = reducer(state, dec(1));
state = reducer(state, inc(3));
state;
```